use demo::{shadertoy::ShaderToyDemo, webgl::WebGlDemo};

use std::path::PathBuf;

use clap::{App, Arg};

fn main() {
    let matches = App::new("Demo Tool")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::with_name("minify")
                .short("m")
                .long("minify")
                .help("Minify HTML."),
        )
        .arg(
            Arg::with_name("target")
                .short("t")
                .long("target")
                .takes_value(true)
                .default_value("webgl")
                .possible_values(&["shadertoy", "webgl"])
                .help("Demo target."),
        )
        .arg(
            Arg::with_name("config")
                .required(true)
                .takes_value(true)
                .help("Path to configuration file."),
        )
        .get_matches();
    let mut path = PathBuf::from(matches.value_of("config").unwrap());
    let file = path.clone();
    path.pop();
    std::env::set_current_dir(path).unwrap();
    match matches.value_of("target") {
        Some("shadertoy") => {
            let demo: ShaderToyDemo = serde_any::from_file(file).unwrap();
            demo.generate().unwrap();
        }
        Some("webgl") => {
            let demo: WebGlDemo = serde_any::from_file(file).unwrap();
            demo.generate().unwrap();
        }
        _ => {}
    }
}
