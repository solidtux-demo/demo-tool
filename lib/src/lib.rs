#![feature(specialization)]

#[macro_use]
extern crate pyo3;

use demo::{shadertoy::ShaderToyDemo, webgl::WebGlDemo};
use pyo3::prelude::*;

py_exception!(demo, JSONParseError, pyo3::exceptions::Exception);
py_exception!(demo, JSONCreateError, pyo3::exceptions::Exception);

#[pyfunction]
fn shadertoy_json(s: String) -> PyResult<()> {
    let d: ShaderToyDemo =
        serde_json::from_str(&s).map_err(|e| PyErr::new::<JSONParseError, _>(format!("{}", e)))?;
    d.generate()
        .map_err(|e| PyErr::new::<JSONParseError, _>(format!("{}", e)))?;
    Ok(())
}

#[pyfunction]
fn shadertoy_default() -> PyResult<String> {
    serde_json::to_string(&ShaderToyDemo::default())
        .map_err(|e| PyErr::new::<JSONCreateError, _>(format!("{}", e)))
}

#[pyfunction]
fn webgl_json(s: String) -> PyResult<()> {
    let d: WebGlDemo =
        serde_json::from_str(&s).map_err(|e| PyErr::new::<JSONParseError, _>(format!("{}", e)))?;
    d.generate()
        .map_err(|e| PyErr::new::<JSONParseError, _>(format!("{}", e)))?;
    Ok(())
}

#[pyfunction]
fn webgl_default() -> PyResult<String> {
    serde_json::to_string(&WebGlDemo::default())
        .map_err(|e| PyErr::new::<JSONCreateError, _>(format!("{}", e)))
}

#[pymodinit]
fn demo(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_function!(shadertoy_json))?;
    m.add_function(wrap_function!(shadertoy_default))?;
    m.add_function(wrap_function!(webgl_json))?;
    m.add_function(wrap_function!(webgl_default))?;

    Ok(())
}
