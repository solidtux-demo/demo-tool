use askama::Template;
use serde_derive::{Deserialize, Serialize};

#[derive(Template, Debug, Serialize, Deserialize)]
#[template(path = "audio.js")]
pub struct Audio {
    pub channels: Vec<Channel>,
    pub length: f32,
}

impl Default for Audio {
    fn default() -> Audio {
        Audio {
            channels: Vec::new(),
            length: 0.,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Channel {
    pub name: String,
    pub oscillator: Oscillator,
    pub frequency: Vec<ParameterChange>,
    pub gain: Vec<ParameterChange>,
}

// TODO custom
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Oscillator {
    Sine,
    Square,
    Sawtooth,
    Triangle,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ParameterChange {
    pub changetype: ChangeType,
    pub value: f32,
    pub time: f32,
}

// TODO target and valuecurve
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum ChangeType {
    Value,
    Linear,
    Exponential,
}

impl std::fmt::Display for ChangeType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{:?}", self)
    }
}
