use askama::Template;
use serde_derive::{Deserialize, Serialize};

pub type GlVec3 = (f32, f32, f32);
pub type GlFloat = f32;
pub type GlInt = i32;

#[derive(Template, Debug, Serialize, Deserialize)]
#[template(path = "shadertoy.glsl")]
pub struct ShaderToyShader {
    pub keyframes: Vec<GlFloat>,
    pub constants: Constants,
    pub camera: Camera,
    pub scene: Scene,
    pub length: Option<GlFloat>,
    pub lights: Vec<Light>,
    pub postprocessing: Vec<PostOperation>,
}

impl Default for ShaderToyShader {
    fn default() -> ShaderToyShader {
        ShaderToyShader {
            keyframes: Vec::new(),
            constants: Constants::default(),
            camera: Camera::default(),
            scene: Scene::default(),
            length: None,
            lights: Vec::new(),
            postprocessing: Vec::new(),
        }
    }
}

#[derive(Template, Debug, Serialize, Deserialize)]
#[template(path = "standalone.glsl")]
pub struct StandaloneShader {
    pub keyframes: Vec<GlFloat>,
    pub constants: Constants,
    pub camera: Camera,
    pub length: Option<GlFloat>,
    pub scene: Scene,
    pub lights: Vec<Light>,
    pub postprocessing: Vec<PostOperation>,
}

impl Default for StandaloneShader {
    fn default() -> StandaloneShader {
        StandaloneShader {
            keyframes: Vec::new(),
            constants: Constants::default(),
            camera: Camera::default(),
            scene: Scene::default(),
            length: None,
            lights: Vec::new(),
            postprocessing: Vec::new(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Constants {
    pub min_depth: GlFloat,
    pub max_depth: GlFloat,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_step: Option<GlFloat>,
    pub max_iter: GlInt,
    pub eps: GlFloat,
    pub shadow_eps: GlFloat,
    pub shadow_depth: GlFloat,
    pub shadow_smooth: GlFloat,
    pub shadow: GlFloat,
}

impl Default for Constants {
    fn default() -> Constants {
        Constants {
            min_depth: 1.,
            max_depth: 50.,
            max_iter: 200,
            max_step: None,
            eps: 0.0001,
            shadow_eps: 0.01,
            shadow_depth: 2.,
            shadow_smooth: 15.,
            shadow: 0.,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Camera {
    pub position: Animation<GlVec3>,
    pub target: Animation<GlVec3>,
    pub up: Animation<GlVec3>,
    pub zoom: Animation<GlFloat>,
}

impl Default for Camera {
    fn default() -> Camera {
        Camera {
            position: Animation::Static((0., 0., 10.)),
            target: Animation::Static((0., 0., 0.)),
            up: Animation::Static((0., 1., 0.)),
            zoom: Animation::Static(1.),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Scene {
    pub operations: Vec<SdfOperation>,
    pub textures: Vec<Texture>,
}

impl Default for Scene {
    fn default() -> Scene {
        Scene {
            operations: Vec::new(),
            textures: Vec::new(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum SdfOperation {
    Set(Object),
    Union(Object),
    Reset,
    Move(Animation<GlVec3>),
    RotateX(Animation<GlFloat>),
    RotateY(Animation<GlFloat>),
    RotateZ(Animation<GlFloat>),
    Rotate(Rotation),
    Expression(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Object {
    pub objecttype: ObjectType,
    pub id: usize,
    pub map: Option<bool>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum ObjectType {
    Sphere { radius: Animation<GlFloat> },
    Box { size: Animation<GlVec3> },
    Plane,
}

impl Object {
    pub fn sdf(&self, pos: &str, id: &usize) -> String {
        match &self.objecttype {
            ObjectType::Sphere { radius } => {
                let uv = if self.map.unwrap_or(false) {
                    format!("map_sphere({pos})", pos = pos)
                } else {
                    "vec2(0., 0.)".to_string()
                };
                format!(
                    "SdfResult(length({pos}) - {radius}, {pos}, {uv}, {id})",
                    pos = pos,
                    uv = uv,
                    radius = radius.generate(),
                    id = id
                )
            }
            ObjectType::Box { size } => {
                let uv = if self.map.unwrap_or(false) {
                    unimplemented!()
                } else {
                    "vec2(0., 0.)".to_string()
                };
                format!(
                    "SdfResult(box({pos}, {size}), {pos}, {uv}, {id})",
                    pos = pos,
                    uv = uv,
                    size = size.generate(),
                    id = id
                )
            }
            ObjectType::Plane => {
                let uv = if self.map.unwrap_or(false) {
                    unimplemented!()
                } else {
                    "vec2(0., 0.)".to_string()
                };
                format!(
                    "SdfResult({pos}.y, {pos}, {uv}, {id})",
                    pos = pos,
                    uv = uv,
                    id = id
                )
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Texture {
    pub texturetype: TextureType,
    pub ambient: Animation<GlFloat>,
    pub diffuse: Animation<GlFloat>,
    pub specular: Animation<GlFloat>,
    pub shininess: Animation<GlFloat>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum TextureType {
    Color(Animation<GlVec3>),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Animation<T> {
    Static(T),
    Smooth(Vec<Animation<T>>),
    Expression(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Light {
    pub position: Animation<GlVec3>,
    pub color: Animation<GlVec3>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum PostOperation {
    Mist(Mist),
    Dark(Mist),
    Expression(String),
    Background(Animation<GlVec3>),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Mist {
    pub density: Animation<GlFloat>,
    pub min_depth: Animation<GlFloat>,
    pub max_depth: Animation<GlFloat>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Rotation {
    pub axis: Animation<GlVec3>,
    pub angle: Animation<GlFloat>,
}

trait ShaderExpression {
    fn generate(&self) -> String;
}

impl<T: ShaderExpression> ShaderExpression for Animation<T> {
    fn generate(&self) -> String {
        match self {
            Animation::Static(x) => x.generate(),
            Animation::Smooth(v) => {
                let mut s = format!(
                    "mix({}, {}, smoothstep(KEYFRAME_0, KEYFRAME_1, TIME))",
                    v[0].generate(),
                    v[1].generate()
                );
                for i in 1..(v.len() - 1) {
                    s = format!(
                        "mix({}, {}, smoothstep(KEYFRAME_{}, KEYFRAME_{}, TIME))",
                        s,
                        v[i + 1].generate(),
                        i,
                        i + 1
                    );
                }
                s
            }
            Animation::Expression(s) => s.clone(),
        }
    }
}

impl ShaderExpression for GlInt {
    fn generate(&self) -> String {
        format!("{}", self)
    }
}

impl ShaderExpression for GlFloat {
    fn generate(&self) -> String {
        format!("float({})", self)
    }
}

impl ShaderExpression for GlVec3 {
    fn generate(&self) -> String {
        format!(
            "vec3(float({}), float({}), float({}))",
            self.0, self.1, self.2
        )
    }
}
