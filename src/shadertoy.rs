use askama::Template;
use failure::Error;
use serde_derive::{Deserialize, Serialize};

use std::{fs::File, io::Write};

use crate::{audio::Audio, shader::ShaderToyShader};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ShaderToyDemo {
    pub name: String,
    pub audio: Audio,
    pub shader: ShaderToyShader,
}

impl ShaderToyDemo {
    pub fn generate(&self) -> Result<(), Error> {
        let mut shader = File::create("video.glsl")?;
        write!(shader, "{}", self.shader.render()?)?;
        Ok(())
    }
}
