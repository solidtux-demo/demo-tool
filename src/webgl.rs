use askama::Template;
use failure::Error;
use serde_derive::{Deserialize, Serialize};

use std::{fs::File, io::Write};

use crate::{audio::Audio, shader::StandaloneShader};

#[derive(Template, Default, Debug, Serialize, Deserialize)]
#[template(path = "webgl.html")]
pub struct WebGlDemo {
    pub name: String,
    pub audio: Audio,
    pub shader: StandaloneShader,
}

impl WebGlDemo {
    pub fn generate(&self) -> Result<(), Error> {
        let mut shader = File::create("video.glsl")?;
        write!(shader, "{}", self.shader.render()?)?;
        let mut html = File::create("index.html")?;
        write!(html, "{}", self.render()?)?;
        Ok(())
    }
}
