{%- for channel in channels %}
var osc{{ channel.name }} = audioCtx.createOscillator();
var gain{{ channel.name }} = audioCtx.createGain();
osc{{ channel.name }}.connect(gain{{ channel.name }});
gain{{ channel.name }}.connect(audioCtx.destination);

osc{{ channel.name }}.type = {%- match channel.oscillator -%}
{%- when Sine -%}
"sine"
{%- when Square -%}
"square"
{%- when Sawtooth -%}
"sawtooth"
{%- endmatch -%};

{%- for change in channel.gain %}
gain{{ channel.name }}.gain.{%- match change.changetype -%}
{%- when ChangeType::Value -%}
setValueAtTime
{%- when ChangeType::Linear -%}
linearRampToValueAtTime
{%- when ChangeType::Exponential -%}
exponentialRampToValueAtTime
{%- endmatch -%}({{ change.value }}, {{ change.time }});
{% endfor -%}
{%- for change in channel.frequency %}
osc{{ channel.name }}.frequency.{%- match change.changetype -%}
{%- when ChangeType::Value -%}
setValueAtTime
{%- when ChangeType::Linear -%}
linearRampToValueAtTime
{%- when ChangeType::Exponential -%}
exponentialRampToValueAtTime
{%- else -%}
{%- endmatch -%}({{ change.value }}, {{ change.time }});
{% endfor -%}
{% endfor -%}
{%- for channel in channels %}
osc{{ channel.name }}.start(0);
osc{{ channel.name }}.stop({{ length }});
{% endfor -%}
