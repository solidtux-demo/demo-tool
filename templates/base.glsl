{% block defines %}{% endblock %}
precision highp float;
{% block uniforms %}{% endblock %}
const float MIN_DEPTH = float({{ constants.min_depth }});
const float MAX_DEPTH = float({{ constants.max_depth }});
const int MAX_ITER = {{ constants.max_iter }};
const float EPS = float({{ constants.eps }});
const float SHADOW = float({{ constants.shadow }});
const float SHADOW_EPS = float({{ constants.shadow_eps }});
const float SHADOW_DEPTH = float({{ constants.shadow_depth }});
const float SHADOW_SMOOTH = float({{ constants.shadow_smooth }});
const float PI = 3.14159265359;
{%- for f in keyframes %}
const float KEYFRAME_{{ loop.index0 }} = {{ f.generate() }};
{%- endfor -%}
{% block constants %}{% endblock %}
struct Ray {
    vec3 origin;
    vec3 direction;
};
struct RayResult {
    float depth;
    int object;
    vec3 norm;
};
struct SdfResult {
    float distance;
    vec3 pos;
    vec2 uv;
    int object;
};
struct Camera {
    vec3 position;
    vec3 target;
    vec3 up;
    float zoom;
};
float rand(float n){
    return fract(sin(n) * 43758.5453123);
}
vec2 norm_coord(in vec2 coord, in vec2 res) {
    vec2 uv = coord.xy/res.xy;
    uv = 2.*uv - 1.;
    if (res.x > res.y) {
        uv.x *= res.x/res.y;
    } else {
        uv.y *= res.y/res.x;
    }
    return uv;
}
vec3 ray_direction(in Camera cam, in vec2 coord, in vec2 res) {
    vec2 uv = norm_coord(coord, res);
    vec3 dir = normalize(cam.target - cam.position);
    vec3 right = normalize(cross(dir, cam.up));
    vec3 u = normalize(cross(right, dir));
    vec3 target = cam.position + normalize(dir)/cam.zoom + uv.x*right + uv.y*u;
    return normalize(target - cam.position);
}
Ray get_ray(in Camera cam, in vec2 coord, in vec2 res) {
    return Ray(cam.position, ray_direction(cam, coord, res));
}
vec2 sphere_map(in vec3 pos) {
    // TODO
    return vec2(0.);
}
float box(vec3 p, vec3 b) {
  vec3 d = abs(p) - b;
  return length(max(d,0.0)) + min(max(d.x,max(d.y,d.z)),0.0);
}
SdfResult sdf_union(in SdfResult a, in SdfResult b) {
    if (a.distance < b.distance) {
        return a;
    } else {
        return b;
    }
}
mat3 rotx(in float a) {
    return mat3(
            1., 0., 0.,
            0., cos(a), -sin(a),
            0., sin(a), cos(a)
            );
}
mat3 roty(in float a) {
    return mat3(
            cos(a), 0., -sin(a),
            0., 1., 0.,
            sin(a), 0., cos(a)
            );
}
mat3 rotz(in float a) {
    return mat3(
            cos(a), -sin(a), 0.,
            sin(a), cos(a), 0.,
            0., 0., 1.
            );
}
SdfResult sdf(in vec3 pos) {
    SdfResult res = SdfResult(MAX_DEPTH, vec3(0.), vec2(0.), -1);
    vec3 p = pos;
    {% for operation in scene.operations %}
    {%- match operation -%}
    {% when SdfOperation::Set with (object) -%}
    res = {{ object.sdf("p", object.id) }};
    {% when SdfOperation::Union with (object) -%}
    res = sdf_union(res, {{ object.sdf("p", object.id) }});
    {% when SdfOperation::Move with (v) -%}
    p += {{ v.generate() }};
    {% when SdfOperation::RotateX with (a) -%}
    p = rotx({{ a.generate() }})*p;
    {% when SdfOperation::RotateY with (a) -%}
    p = roty({{ a.generate() }})*p;
    {% when SdfOperation::RotateZ with (a) -%}
    p = rotz({{ a.generate() }})*p;
    {% when SdfOperation::Rotate with (r) -%}
    // TODO
    {% when SdfOperation::Reset -%};
    p = pos;
    {% when SdfOperation::Expression with (s) -%}
    {{ s }}
    {%- endmatch -%}
    {% endfor %}
    return res;
}
vec3 norm(in vec3 p) {
    return normalize(vec3(
        sdf(vec3(p.x + EPS, p.y, p.z)).distance - sdf(vec3(p.x - EPS, p.y, p.z)).distance,
        sdf(vec3(p.x, p.y + EPS, p.z)).distance - sdf(vec3(p.x, p.y - EPS, p.z)).distance,
        sdf(vec3(p.x, p.y, p.z + EPS)).distance - sdf(vec3(p.x, p.y, p.z - EPS)).distance
    ));
}
RayResult raymarcher(in Ray ray) {
    float depth = MIN_DEPTH;
    for (int i=0; i<MAX_ITER; i++) {
        vec3 pos = ray.origin + depth*ray.direction;
        SdfResult res = sdf(pos);
        if (res.distance < EPS) {
            return RayResult(depth, res.object, norm(pos));
        }
        {%- match constants.max_step -%}
        {% when Some with (d) -%}
        depth += min(float({{ d }}), res.distance);
        {% when None -%}
        depth += res.distance;
        {% endmatch -%}
        if (depth > MAX_DEPTH) {
            return RayResult(MAX_DEPTH, -1, vec3(0.));
        }
    }
    return RayResult(MAX_DEPTH, -1, vec3(0.));
}
float lightmarch(in Ray ray, in float maxd, in float k) {
    float depth = 0.;
    float r = 1.;
    for (int i=0; i<MAX_ITER; i++) {
        vec3 pos = ray.origin + depth*ray.direction;
        SdfResult res = sdf(pos);
        if (res.distance < SHADOW_EPS) {
            return 0.;
        }
        r = min(r, k*res.distance/depth);
        depth += res.distance;
        if (depth >= (maxd - SHADOW_DEPTH)) {
            return r;
        }
    }
    return r;
}
vec4 color(in vec2 coord, in vec2 resolution) {
    Camera cam = Camera({{ camera.position.generate() }}, {{ camera.target.generate() }}, {{ camera.up.generate() }}, {{ camera.zoom.generate() }});
    Ray ray = get_ray(cam, coord, resolution);
    RayResult res = raymarcher(ray);
    vec3 pos = ray.origin + res.depth*ray.direction;
    vec3 col;
    {%- for text in scene.textures %}
    if (res.object == {{ loop.index0 }}) {
        {% match text.texturetype -%}
        {%- when TextureType::Color with (col) -%}
        vec3 amb = {{ col.generate() }};
        {%- endmatch %}
        vec3 spec = vec3(0.);
        vec3 diff = vec3(0.);
        vec3 h;
        vec3 l;
        vec3 lpos;
        vec3 ldir;
        float bright = 1.;
        {%- for light in lights %}
        lpos = {{ light.position.generate() }};
        l = normalize(lpos - pos);
        h = normalize(-ray.direction + l);
        spec += {{ light.color.generate() }}*pow(dot(res.norm, h), {{ text.shininess.generate() }});
        diff += amb*dot(l, res.norm);
        if (SHADOW > 0.) {
            Ray light_ray = Ray(lpos, -l);
            float s = SHADOW*lightmarch(light_ray, length(lpos - pos), SHADOW_SMOOTH) + 1. - SHADOW;
            spec *= s;
            diff *= s;
        }
        {% endfor -%}
        col = {{ text.ambient.generate() }}*amb + {{ text.diffuse.generate() }}*diff + {{ text.specular.generate() }}*spec;
    }
    {% endfor -%}
    {%- for operation in postprocessing %}
    {%- match operation -%}
    {%- when PostOperation::Mist with (m) -%}
    col += {{ m.density.generate() }} * smoothstep({{ m.min_depth.generate() }}, {{ m.max_depth.generate() }}, res.depth);
    {%- when PostOperation::Dark with (m) -%}
    col -= {{ m.density.generate() }} * smoothstep({{ m.min_depth.generate() }}, {{ m.max_depth.generate() }}, res.depth);
    {%- when PostOperation::Expression with (s) -%}
    {{ s }}
    {%- when PostOperation::Background with (c) -%}
    if (res.object == -1) {
        col = {{ c.generate() }};
    }
    {%- endmatch -%}
    {%- endfor %}
    col = clamp(col, 0., 1.);
    return vec4(col, 1.);
}
{% block main %}{% endblock %}
