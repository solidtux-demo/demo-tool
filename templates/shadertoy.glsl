{% extends "base.glsl" %}
{% block defines %}
{% match length -%}
{% when Some with (l) -%}
#define TIME mod(iTime, {{ l.generate() }})
{% when None -%}
#define TIME iTime
{%- endmatch %}
{% endblock %}
{% block main %}
void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    fragColor = color(fragCoord, iResolution.xy);
}
{% endblock %}
