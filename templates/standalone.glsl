{% extends "base.glsl" %}
{% block defines %}
{% match length -%}
{% when Some with (l) -%}
#define TIME mod(iTime, {{ l.generate() }})
{% when None -%}
#define TIME iTime
{%- endmatch %}
{% endblock %}
{% block uniforms %}
uniform float iTime;
uniform vec2 iResolution;
{% endblock %}
{% block main %}
void main() {
    gl_FragColor = color(gl_FragCoord.xy, iResolution);
}
{% endblock %}
